#pragma once

#include <iostream>

using namespace std;

class TicTacToe
{

private:

	char m_board[10];
	int m_numTurns = 0;
	char m_playerTurn = 'X';
	char m_winner;

public:

	// constructor
	TicTacToe() { }


	// methods
	void DisplayBoard()
	{
		cout << "\n";
		cout << "| " << m_board[1] << " | " << m_board[2] << " | " << m_board[3] << " |\n";
		cout << "|___|___|___|\n";
		cout << "| " << m_board[4] << " | " << m_board[5] << " | " << m_board[6] << " |\n";
		cout << "|___|___|___|\n";
		cout << "| " << m_board[7] << " | " << m_board[8] << " | " << m_board[9] << " |\n";
		cout << "|___|___|___|\n";
	};

	bool IsOver()
	{

		// win conditions
		if ((m_board[1] == 'X' && m_board[2] == 'X' && m_board[3] == 'X') || (m_board[1] == 'O' && m_board[2] == 'O' && m_board[3] == 'O'))
		{
			m_winner = m_board[1];
			return true;
		}
		else if ((m_board[4] == 'X' && m_board[5] == 'X' && m_board[6] == 'X') || (m_board[4] == 'O' && m_board[5] == 'O' && m_board[6] == 'O'))
		{
			m_winner = m_board[4];
			return true;
		}
		else if ((m_board[7] == 'X' && m_board[8] == 'X' && m_board[9] == 'X') || (m_board[7] == 'O' && m_board[8] == 'O' && m_board[9] == 'O'))
		{
			m_winner = m_board[7];
			return true;
		}
		else if ((m_board[1] == 'X' && m_board[4] == 'X' && m_board[7] == 'X') || (m_board[1] == 'O' && m_board[4] == 'O' && m_board[7] == 'O'))
		{
			m_winner = m_board[1];
			return true;
		}
		else if ((m_board[2] == 'X' && m_board[5] == 'X' && m_board[8] == 'X') || (m_board[2] == 'O' && m_board[5] == 'O' && m_board[8] == 'O'))
		{
			m_winner = m_board[2];
			return true;
		}
		else if ((m_board[3] == 'X' && m_board[6] == 'X' && m_board[9] == 'X') || (m_board[3] == 'O' && m_board[6] == 'O' && m_board[9] == 'O'))
		{
			m_winner = m_board[3];
			return true;
		}
		else if ((m_board[1] == 'X' && m_board[5] == 'X' && m_board[9] == 'X') || (m_board[1] == 'O' && m_board[5] == 'O' && m_board[9] == 'O'))
		{
			m_winner = m_board[1];
			return true;
		}
		else if ((m_board[3] == 'X' && m_board[5] == 'X' && m_board[7] == 'X') || (m_board[3] == 'O' && m_board[5] == 'O' && m_board[7] == 'O'))
		{
			m_winner = m_board[3];
			return true;
		}

		// full board with no win condition met
		else if (m_numTurns == 9)
		{
			return true;
		}

		// else keep playing
		else
		{
			return false;
		}
	};

	char GetPlayerTurn()
	{
		if (m_playerTurn == 'X')
		{
			return 'X';
		}
		else if (m_playerTurn == 'O')
		{
			return 'O';
		}
	};

	bool IsValidMove(int position)
	{
		if (position >= 1 && position <= 9)
		{
			if (m_board[position] != 'X' && m_board[position] != 'O')
			{
				return true;
			}
			else
			{
				cout << "Invalid move.\n";
				return false;
			}
		}
		else
		{
			cout << "Invalid input.\n";
			return false;
		}
	};

	void Move(int position)
	{
		if (m_playerTurn == 'X')
		{
			m_board[position] = 'X';
			m_playerTurn = 'O';
			m_numTurns++;
		}
		else if (m_playerTurn == 'O')
		{
			m_board[position] = 'O';
			m_playerTurn = 'X';
			m_numTurns++;
		}

	};

	void DisplayResult()
	{
		if (m_numTurns == 9)
		{
			cout << "Tie!" << endl;
		}
		else
		{
			cout << m_winner << " has won!" << endl;
		}

	};

};